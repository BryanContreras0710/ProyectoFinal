package inv.project.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import inv.project.model.Hotel;

import inv.project.service.HotelService;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class HotelController {
	
	@Autowired
	private HotelService hotelService;

	//Agregar nueva hotel
	@PostMapping("/hotel")
	public ResponseEntity<?> save(@RequestBody Hotel hotel) {
		long id = hotelService.save(hotel);
		return ResponseEntity.ok()
				.body("New hotel has been saved with ID:" + id);
	}
	
	//Obtener hoteles por id
		@GetMapping("/hoteles")
		public ResponseEntity<List<Hotel>> get(){
			List<Hotel> hoteles = hotelService.list();
			return ResponseEntity.ok().body(hoteles);
		}
	//Obtener hotel por id
	@GetMapping("/hotel/{id}")
	public ResponseEntity<Hotel> get(@PathVariable("id") int id) {
		Hotel hotel = hotelService.get(id);
		return ResponseEntity.ok().body(hotel);
	}
	
	 /*---Update a hotel by id---*/
	   @PutMapping("/hotel/{id}")
	   public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody Hotel hotel) {
		   hotelService.update(id, hotel);
	      return ResponseEntity.ok().body("cambio exitoso");
	   }
	   
	   /*---Delete a hotel by id---*/
	   @DeleteMapping("/hotel/{id}")
	   public ResponseEntity<?> delete(@PathVariable("id") int 	 id) {
		   hotelService.delete(id);
	      return ResponseEntity.ok().body("borrado correctamente");
	   }

}
