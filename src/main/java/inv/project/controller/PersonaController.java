package inv.project.controller;

import inv.project.model.Persona;
import inv.project.service.PersonaService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class PersonaController {

	@Autowired
	private PersonaService personaService;

	//Agregar nueva persona
	@PostMapping("/persona")
	public ResponseEntity<?> save(@RequestBody Persona persona) {
		long id_persona = personaService.save(persona);
		return ResponseEntity.ok()
				.body("New persona has been saved with ID:" + id_persona);
	}
    
	//Obtener personas por id
	@GetMapping("/personas")
	public ResponseEntity<List<Persona>> get(){
		List<Persona> personas = personaService.list();
		return ResponseEntity.ok().body(personas);
		
	}
	//Obtener persona por id
	@GetMapping("/persona/{id_persona}")
	public ResponseEntity<Persona> get(@PathVariable("id_persona") int id_persona) {
		Persona persona = personaService.get(id_persona);
		return ResponseEntity.ok().body(persona);
	}
	
	 /*---Update a persona by id---*/
	   @PutMapping("/persona/{id_persona}")
	   public ResponseEntity<?> update(@PathVariable("id_persona") int id_persona, @RequestBody Persona persona) {
	      personaService.update(id_persona, persona);
	      return ResponseEntity.ok().body("cambio exitoso");
	   }
	   
	   /*---Delete a persona by id---*/
	   @DeleteMapping("/persona/{id_persona}")
	   public ResponseEntity<?> delete(@PathVariable("id_persona") int 	 id_persona) {
	      personaService.delete(id_persona);
	      return ResponseEntity.ok().body("borrado correctamente");
	   }
}
