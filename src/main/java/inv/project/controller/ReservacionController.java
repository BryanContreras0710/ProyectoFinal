package inv.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import inv.project.model.Reservacion;
import inv.project.service.ReservacionService;
@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class ReservacionController {

	@Autowired
	private ReservacionService reservacionService;

	//Agregar nueva reservacion
	@PostMapping("/reservacion")
	public ResponseEntity<?> save(@RequestBody Reservacion reservacion) {
		long id = reservacionService.save(reservacion);
		return ResponseEntity.ok()
				.body("New reservacion has been saved with ID:" + id);
	}
	
	@GetMapping("/reservaciones")
	public ResponseEntity<List<Reservacion>> get(){
		List<Reservacion> reservaciones = reservacionService.list();
		return ResponseEntity.ok().body(reservaciones);
	}
	//Obtener reservacion por id
	@GetMapping("/reservacion/{id}")
	public ResponseEntity<Reservacion> get(@PathVariable("id") int id) {
		Reservacion reservacion = reservacionService.get(id);
		return ResponseEntity.ok().body(reservacion);
	}
	
	 /*---Update a reservacion by id---*/
	   @PutMapping("/reservacion/{id}")
	   public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody Reservacion reservacion) {
		   reservacionService.update(id, reservacion);
	      return ResponseEntity.ok().body("cambio exitoso");
	   }
	   
	   /*---Delete a reservacion by id---*/
	   @DeleteMapping("/reservacion/{id}")
	   public ResponseEntity<?> delete(@PathVariable("id") int 	 id) {
	      reservacionService.delete(id);
	      return ResponseEntity.ok().body("borrado correctamente");
	   }
}
