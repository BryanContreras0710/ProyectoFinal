package inv.project.dao;

import java.util.List;

import inv.project.model.Hotel;

public interface HotelDao {

	Integer save(Hotel hotel);

	Hotel get(Integer id);

	List<Hotel> list();

	void update(Integer id, Hotel hotel);

	void delete(Integer id);
}
