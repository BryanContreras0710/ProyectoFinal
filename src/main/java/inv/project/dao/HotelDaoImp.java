package inv.project.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import inv.project.model.Hotel;


@Repository
public class HotelDaoImp implements HotelDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Integer save(Hotel hotel) {
		sessionFactory.getCurrentSession().save(hotel);
		return hotel.getId_hotel();
	}

	public Hotel get(Integer id) {
		return sessionFactory.getCurrentSession().get(Hotel.class, id);
	}
	@SuppressWarnings("unchecked")
	public List<Hotel> list() {
		Session session = sessionFactory.getCurrentSession();
		Query<Hotel> query = session.createQuery("from Hotel");
		List<Hotel> hoteles = query.list();
		return hoteles;
	}

	public void update(Integer id, Hotel hotel) {
		Session session = sessionFactory.getCurrentSession();
		Hotel hotelActualizada = session.byId(Hotel.class).load(id);
		hotelActualizada.setNom_hotel(hotel.getNom_hotel());
		hotelActualizada.setHabitacion(hotel.getHabitacion());
		hotelActualizada.setDireccion(hotel.getDireccion());
		hotelActualizada.setTelefono(hotel.getTelefono());
		session.flush();
	}

	public void delete(Integer id) {
		Session session = sessionFactory.getCurrentSession();
        Hotel hotel = session.byId(Hotel.class).load(id);
		session.delete(hotel);
	}
}