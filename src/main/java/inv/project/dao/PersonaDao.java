package inv.project.dao;

import inv.project.model.Persona;

import java.util.List;

public interface PersonaDao {

	Integer save(Persona persona);

	Persona get(Integer id);

	List<Persona> list();

	void update(Integer id, Persona persona);

	void delete(Integer id);
}
