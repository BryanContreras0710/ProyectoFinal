package inv.project.dao;

import java.util.List;

import inv.project.model.Reservacion;

public interface ReservacionDao {

	Integer save(Reservacion reservacion);

	Reservacion get(Integer id);

	List<Reservacion> list();

	void update(Integer id, Reservacion reservacion);

	void delete(Integer id);
}
