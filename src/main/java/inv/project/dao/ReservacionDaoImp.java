package inv.project.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import inv.project.model.Reservacion;

@Repository
public class ReservacionDaoImp implements ReservacionDao {

	@Autowired
	private SessionFactory sessionFactory;
	

	public Integer save(Reservacion reservacion) {
		sessionFactory.getCurrentSession().save(reservacion);
		return reservacion.getId();
	}

	public Reservacion get(Integer id) {
		return sessionFactory.getCurrentSession().get(Reservacion.class, id);
	}
	@SuppressWarnings("unchecked")
	public List<Reservacion> list() {
		Session session = sessionFactory.getCurrentSession();
		Query<Reservacion> query = session.createQuery("from Reservacion");
		List<Reservacion> reservaciones = query.list();
		return reservaciones;
	}

	public void update(Integer id, Reservacion reservacion) {
		Session session = sessionFactory.getCurrentSession();
		Reservacion reservacionActualizada = session.byId(Reservacion.class).load(id);
		reservacionActualizada.setF_entrada(reservacion.getF_entrada());
		reservacionActualizada.setF_salida(reservacion.getF_salida());
		reservacionActualizada.setPersona(reservacion.getPersona());
		reservacionActualizada.setHotel(reservacion.getHotel());
		session.flush();
	}

	public void delete(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Reservacion reservacion = session.byId(Reservacion.class).load(id);
		session.delete(reservacion);
	}

}

