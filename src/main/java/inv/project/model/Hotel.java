package inv.project.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "Hotel")
public class Hotel  {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_hotel;
    private String nom_hotel;
	private Integer habitacion;
	private String direccion;
	private Integer telefono;
	

	public Integer getId_hotel() {
		return id_hotel;
	}
	public void setId_hotel(Integer id_hotel) {
		this.id_hotel = id_hotel;
	}
	public String getNom_hotel() {
		return nom_hotel;
	}
	public void setNom_hotel(String nom_hotel) {
		this.nom_hotel = nom_hotel;
	}
	public Integer getHabitacion() {
		return habitacion;
	}
	public void setHabitacion(Integer habitacion) {
		this.habitacion = habitacion;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public Integer getTelefono() {
		return telefono;
	}
	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}
	
	
}
