package inv.project.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "Persona")
public class Persona  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_persona;
	private String nom_persona;
	private String apellido;
	private Integer edad;
	

	public Integer getId_persona() {
		return id_persona;
	}
	public void setId_persona(Integer id_persona) {
		this.id_persona = id_persona;
	}
	public String getNom_persona() {
		return nom_persona;
	}
	public void setNom_persona(String nom_persona) {
		this.nom_persona = nom_persona;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}

}
