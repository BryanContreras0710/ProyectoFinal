package inv.project.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity(name = "Reservacion")
public class Reservacion  {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String f_entrada;
	private String f_salida;
	
	@OneToOne
	private Persona persona;
	
	@OneToOne
	private Hotel hotel;
		
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getF_entrada() {
		return f_entrada;
	}

	public void setF_entrada(String f_entrada) {
		this.f_entrada = f_entrada;
	}

	public String getF_salida() {
		return f_salida;
	}

	public void setF_salida(String f_salida) {
		this.f_salida = f_salida;
	}
	public Persona getPersona() {
		return persona;
	}
	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	public Hotel getHotel() {
		return hotel;
	}
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	
	
	
}
