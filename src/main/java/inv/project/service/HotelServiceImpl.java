package inv.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import inv.project.dao.HotelDao;
import inv.project.model.Hotel;

@Service
@Transactional(readOnly = true)
public class HotelServiceImpl implements HotelService {

	@Autowired
	private HotelDao hotelDao;

	@Transactional
	public Integer save(Hotel hotel) {
		return hotelDao.save(hotel);
	}

	public Hotel get(Integer id) {
		return hotelDao.get(id);
	}

	public List<Hotel> list() {
		return hotelDao.list();
		
	}
	@Transactional
	public void update(Integer id, Hotel hotel) {
		hotelDao.update(id, hotel);

	}
	@Transactional
	public void delete(Integer id) {
		hotelDao.delete(id);

	}

}