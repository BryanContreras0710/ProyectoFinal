package inv.project.service;

import java.util.List;

import inv.project.model.Reservacion;

public interface ReservacionService {
	
	Integer save(Reservacion reservacion);

	Reservacion get(Integer id);

	List<Reservacion> list();

	void update(Integer id, Reservacion reservacion);

	void delete(Integer id);

}
