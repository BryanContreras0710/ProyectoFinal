package inv.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import inv.project.dao.ReservacionDao;
import inv.project.model.Reservacion;

	@Service
	@Transactional(readOnly = true)
	public class ReservacionServiceImpl implements ReservacionService {

		@Autowired
		private ReservacionDao reservacionDao;

		@Transactional
		public Integer save(Reservacion reservacion) {
			return reservacionDao.save(reservacion);
		}

		public Reservacion get(Integer id) {
			return reservacionDao.get(id);
		}

		public List<Reservacion> list() {
			return reservacionDao.list();
			
		}
		@Transactional
		public void update(Integer id, Reservacion reservacion) {
			reservacionDao.update(id, reservacion);

		}
		@Transactional
		public void delete(Integer id) {
			reservacionDao.delete(id);

		}

	}

